variable "application" {
  type        = string
  description = "Name of the application the resource(s) will be a part of."
}

variable "environment" {
  type        = string
  description = "Name of the environment the resource(s) will be a part of."
}

variable "role" {
  type        = string
  description = "Name of the role the resource(s) will be performing."
}

variable "subnet_type" {
  type        = string
  description = "Specifies which subnet, either `public`, `private`, or `data`, the instance are launched in."
}

variable "iam_role_id" {
  type        = string
  description = "ID of the IAM role for the EC2 instance(s) which policies are attached to."
}
