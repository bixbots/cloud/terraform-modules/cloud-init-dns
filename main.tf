data "aws_vpc" "primary" {
  tags = {
    "Name" = var.environment
  }
}

data "aws_route53_zone" "primary" {
  name = data.aws_vpc.primary.tags["DNSZone"]
}

module "core" {
  source = "./core"

  zone_name   = data.aws_route53_zone.primary.name
  record_name = "${var.application}-${var.role}"
  subnet_type = var.subnet_type
  iam_role_id = var.iam_role_id
}
