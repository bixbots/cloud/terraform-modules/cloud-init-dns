data "aws_caller_identity" "current" {}

data "aws_route53_zone" "primary" {
  name = var.zone_name
}

locals {
  dns_zone = data.aws_route53_zone.primary.name
  dns_fqdn = "${var.record_name}.${local.dns_zone}"
}
