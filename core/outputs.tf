output "zone_id" {
  description = "The ID of the Route53 zone."
  value       = data.aws_route53_zone.primary.zone_id
}

output "zone_name" {
  description = "The name of the Route53 zone."
  value       = data.aws_route53_zone.primary.name
}

output "record_name" {
  description = "The name of the DNS record."
  value       = var.record_name
}

output "fqdn" {
  description = "The fully qualified domain name of the DNS record."
  value       = local.dns_fqdn
}

output "part" {
  description = "The rendered cloud init script to be run on the instance(s) after boot."
  value = {
    filename     = "update-dns.sh"
    content_type = "text/x-shellscript"
    content = templatefile("${path.module}/update-dns.sh", {
      dns_hostname_prefix = var.subnet_type == "public" ? "public-" : ""
      dns_zone_id         = data.aws_route53_zone.primary.zone_id
      dns_fqdn            = local.dns_fqdn
    })
  }
}
