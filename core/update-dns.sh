#!/usr/bin/env bash

export DNS_HOSTNAME=$(curl -s http://169.254.169.254/latest/meta-data/${dns_hostname_prefix}hostname)
export DNS_ZONE_ID="${dns_zone_id}"
export DNS_FQDN="${dns_fqdn}"

mkdir -p /opt/dns

cat > /opt/dns/dns.json <<EOF
{
    "Comment": "Update record to reflect new IP address of EC2 instance",
    "Changes": [
        {
            "Action": "UPSERT",
            "ResourceRecordSet": {
                "Name": "$${DNS_FQDN}",
                "Type": "CNAME",
                "TTL": 300,
                "ResourceRecords": [
                    {
                        "Value": "$${DNS_HOSTNAME}"
                    }
                ]
            }
        }
    ]
}
EOF

cat > /opt/dns/dns-remove.json <<EOF
{
    "Comment": "Update record to reflect new IP address of EC2 instance",
    "Changes": [
        {
            "Action": "DELETE",
            "ResourceRecordSet": {
                "Name": "$${DNS_FQDN}",
                "Type": "CNAME",
                "TTL": 300,
                "ResourceRecords": [
                    {
                        "Value": "$${DNS_HOSTNAME}"
                    }
                ]
            }
        }
    ]
}
EOF

cat > /etc/init.d/update-dns <<EOF
#!/usr/bin/env bash
#
#	/etc/rc.d/init.d/update-dns
#
#	Creates or updates Route53 record on startup
#   Deletes Route53 record on stop

# chkconfig: 345 26 74
# description: Start script for launch my service

### BEGIN INIT INFO
# Provides:          Route53 update/purge
# Required-Start:    \$remote_fs \$syslog
# Required-Stop:     \$remote_fs \$syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Creates or updates route53 record on start, deletes it on stop
# Description:       Creates or updates route53 record on start, deletes it on stop
### END INIT INFO

start() {
    echo -n "Creating or updating route53 record: "
    touch /var/lock/subsys/update-dns
    aws route53 change-resource-record-sets --hosted-zone-id "$${DNS_ZONE_ID}" --change-batch file:///opt/dns/dns.json
    return \$?
}

stop() {
    echo -n "Deleting route53 record: "
    rm -rf /var/lock/subsys/update-dns
    aws route53 change-resource-record-sets --hosted-zone-id "$${DNS_ZONE_ID}" --change-batch file:///opt/dns/dns-remove.json
    return \$?
}

case "\$1" in
    start)
    start
    ;;
    stop)
    stop
    ;;
    *)
    echo "Usage: \$0 {start|stop}"
    exit 1
    ;;
esac
EOF

chmod +x /etc/init.d/update-dns
chkconfig --add update-dns
chkconfig update-dns --level 12345 on
chkconfig update-dns --level 06 off
service update-dns start
