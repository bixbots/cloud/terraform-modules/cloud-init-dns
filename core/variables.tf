variable "zone_name" {
  type        = string
  description = "The name of the Route53 zone."
}

variable "record_name" {
  type        = string
  description = "The name of DNS record."
}

variable "subnet_type" {
  type        = string
  description = "Specifies which subnet, either `public`, `private`, or `data`, the instance are launched in."
}

variable "iam_role_id" {
  type        = string
  description = "ID of the IAM role for the EC2 instance which policies are attached to."
}
