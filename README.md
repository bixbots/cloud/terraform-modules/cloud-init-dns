# cloud-init-dns

## Summary

Exports a cloud-init script to update an EC2 instance's DNS record in Route53 when it launches. The DNS record is formatted as `{application}-{role}` in the environment's associated Route53 zone. A `core` submodule is available to provide a custom DNS record name.

## Resources

This module doesn't create any resources.

## Cost

Using this module on its own doesn't cost anything.

## Inputs

| Name        | Description                                                                                  | Type     | Default | Required |
|:------------|:---------------------------------------------------------------------------------------------|:---------|:--------|:---------|
| application | Name of the application the resource(s) will be a part of.                                   | `string` | -       | yes      |
| environment | Name of the environment the resource(s) will be a part of.                                   | `string` | -       | yes      |
| role        | Name of the role the resource(s) will be performing.                                         | `string` | -       | yes      |
| subnet_type | Specifies which subnet, either `public`, `private`, or `data`, the instance are launched in. | `string` | -       | yes      |
| iam_role_id | ID of the IAM role for the EC2 instance(s) which policies are attached to.                   | `string` | -       | yes      |

## Outputs

| Name        | Description                                                                   |
|:------------|:------------------------------------------------------------------------------|
| zone_id     | The ID of the Route53 zone.                                                   |
| zone_name   | The name of the Route53 zone.                                                 |
| record_name | The name of the DNS record.                                                   |
| fqdn        | The fully qualified domain name of the DNS record.                            |
| part        | The rendered cloud init script to be run on the instance(s) when it launches. |

## Examples

### Simple Usage

```terraform
module "cloud_init_dns" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/cloud-init-dns.git?ref=v1"

  application = "example_app"
  environment = "example_env"
  role        = "example_role"
  subnet_type = "public"
  iam_role_id = "todo"
}

data "template_cloudinit_config" "example" {
  part {
    filename     = module.cloud_init_dns.part.filename
    content_type = module.cloud_init_dns.part.content_type
    content      = module.cloud_init_dns.part.content
  }
}

resource "aws_instance" "example" {
  user_data              = data.template_cloudinit_config.example.rendered

  # other properties omitted for brevity
  # ...
}
```

## Version History

* v1 - Initial Release
