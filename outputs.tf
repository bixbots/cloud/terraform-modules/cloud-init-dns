output "zone_id" {
  description = "The ID of the Route53 zone."
  value       = module.core.zone_id
}

output "zone_name" {
  description = "The name of the Route53 zone."
  value       = module.core.zone_name
}

output "record_name" {
  description = "The name of the DNS record."
  value       = module.core.record_name
}

output "fqdn" {
  description = "The fully qualified domain name of the DNS record."
  value       = module.core.fqdn
}

output "part" {
  description = "The rendered cloud init script to be run on the instance(s) when it launches."
  value       = module.core.part
}
